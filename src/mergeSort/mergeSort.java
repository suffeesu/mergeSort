package mergeSort;

/**
 * Created by suffee on 2017/4/8.
 */
public class mergeSort
{
    public  void mergeSort(int mount,int[] data)
    {

        if(mount>1)
        {
            final int left = mount/2 ;
            final int right =mount-left  ;

            int[] leftArray =new int[left];
            int[] rightArray=new int[right];

            mergeSort(left,leftArray);
            mergeSort(right,rightArray);
            merge(left,right,leftArray,rightArray,data);
        }

    }

    public void merge(int left,int right,final int[] leftArray,final int[] rightArray,int[] data)
    {
        int leftPtr = 1;
        int rightPtr= 1;
        int mainPtr = 1;

        while ((leftPtr <= left) && (rightPtr <= right) )
        {
            if(leftArray[leftPtr] < rightArray[rightPtr])//Compare left and right
            {
                data[mainPtr] = leftArray[leftPtr];
                leftPtr++;
            }
            else {
                data[mainPtr] = rightArray[rightPtr];
                rightPtr++;
            }
            mainPtr++;
        }

        if(leftPtr > left)                              //make left[rightPtr]-left[right] into main[mainptr]-main[endl]
            for(int i=0;i<right-rightPtr;i++)
                data[mainPtr+i]=leftArray[rightPtr+i];
        else                                            //make Right[leftptr]-Right[left]  into main[mainptr]-main[endl]
            for(int i=0;i<left-leftPtr;i++)
                data[mainPtr+i]=leftArray[leftPtr+i];


    }

}
